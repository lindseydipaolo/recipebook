import React from 'react';
import BackButton from '../BackButton';

function PumpkinPieOatmeal() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>1 cup steel cut oats</li>
          <li>2 1/2 cups water</li>
          <li>1 1/2 cups oat milk</li>
          <li>1 cup canned pumpkin</li>
          <li>3 Tbs real maple syrup</li>
          <li>1 tsp vanilla extract</li>
          <li>1 tsp pumpkin pie spice</li>
          <li>1/2 tsp cinnamon</li>
          <li>1/4 tsp salt</li>
          <li>mixed nuts</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Spray a slow cooker with non-stick spray.</li>
        <li>Add all the ingredients, except for the nuts, to the slow cooker and stir.</li>
        <li>Cook on low for 7 hours.</li>
        <li>Once done, top with mixed nuts.</li>
    </div>
  );
}

export default PumpkinPieOatmeal;
