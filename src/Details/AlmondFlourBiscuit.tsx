import React from 'react';
import BackButton from '../BackButton';

function AlmondFlourBiscuit() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>1/2 cup almond flour</li>
          <li>2 eggs</li>
          <li>3/4 tsp olive oil</li>
          <li>3/4 tsp water</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Mix the ingredients in a small bowl.</li>
        <li>Microwave for 2 minutes.</li>
        <li>Remove the biscuit from the bowl, then place it in a pan over medium heat.</li>
        <li>Cook each side until golden.</li>
    </div>
  );
}

export default AlmondFlourBiscuit;
