import React from 'react';
import BackButton from '../BackButton';

function AlmondFlourBread() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <ul>dry ingredients</ul>
          <li>2 cups almond flour (loosely packed)</li>
          <li>3 Tbs coconut flour</li>
          <li>1/2 tsp baking powder</li>
          <li>1/2 tsp baking soda</li>
          <li>1/2 heaping tsp salt</li>
          <ul>wet ingredients</ul>
          <li>4 eggs</li>
          <li>1/3 cup oat milk</li>
          <li>1/4 cup melted butter</li>
          <li>3 Tbs honey</li>
          <li>1 tsp apple cider vinegar</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Mix the dry ingredients together.</li>
        <li>Mix the wet ingredients together.</li>
        <li>Pour the wet ingredients into the dry ingredients and stir until combined.</li>
        <li>Bake at 350 in a 9-inch diameter pan for 25 minutes (or until top is golden).</li>
        <li>Let cool for ~15 minutes.</li>
    </div>
  );
}

export default AlmondFlourBread;
