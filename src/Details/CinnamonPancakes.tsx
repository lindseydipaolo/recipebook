import React from 'react';
import BackButton from '../BackButton';

function CinnamonPancakes() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>1/2 cup almond flour</li>
          <li>1/4 cup coconut flour</li>
          <li>1/3 cup coconut yogurt</li>
          <li>4 eggs</li>
          <li>1 tsp baking powder</li>
          <li>1 1/2 tsp cinnamon</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Starting with the eggs, add all ingredients to a blender.</li>
        <li>Blend until the ingredients are mixed.</li>
        <li>Heat a pan over medium heat and spray with non-stick spray.</li>
        <li>Cook the pancakes on each side until they are golden brown on either side and firm in the middle.</li>
    </div>
  );
}

export default CinnamonPancakes;
