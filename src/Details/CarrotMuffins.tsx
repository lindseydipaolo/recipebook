import React from 'react';
import BackButton from '../BackButton';

function CarrotMuffins() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>2 cups baby carrots</li>
          <li>1/2 cup Old Fashioned oats</li>
          <li>4 eggs</li>
          <li>1 1/2 tsp baking powder</li>
          <li>2 tsp cinnamon</li>
          <li>1/4 tsp nutmeg</li>
          <li>1/2 tsp vanilla extract</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Blend the ingredients in a blender.</li>
        <li>Spray a muffin tin with non-stick spray, then pour the ingredients into it.</li>
        <li>Bake for 22 min at 350 Fahrenheit.</li>
        <li>Let the muffins cool, then top with honey and cinnamon.</li>
    </div>
  );
}

export default CarrotMuffins;
