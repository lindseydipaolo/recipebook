import React from 'react';
import BackButton from '../BackButton';

function LemonMuffins() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <ul>dry ingredients</ul>
          <li>1/2 cup coconut flour</li>
          <li>2 Tbs chia seeds</li>
          <li>1/4 tsp baking soda</li>
          <li>1/16 tsp salt</li>
          <ul>wet ingredients</ul>
          <li>1/2 cup oat milk</li>
          <li>4 eggs</li>
          <li>2 lemons (zest and juice)</li>
          <li>2 Tbs olive oil</li>
          <li>1 Tbs apple cider vinegar</li>
          <li>3 Tbs honey</li>
          <li>1/4 tsp vanilla extract</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Add the dry ingredients to a large bowl and mix.</li>
        <li>Add the wet ingredients to a medium-sized bowl and mix.</li>
        <li>Add the wet ingredients to the dry ingredients and stir until smooth.</li>
        <li>Spray a muffin tin with non-stick spray and add the mix to each section.</li>
        <li>Bake for 20-25 minutes at 350 degrees, then let the muffins cool.</li>
    </div>
  );
}

export default LemonMuffins;
