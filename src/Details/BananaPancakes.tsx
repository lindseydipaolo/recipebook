import React from 'react';
import BackButton from '../BackButton';

function BananaPancakes() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>1/2 cup oat milk</li>
          <li>2 eggs</li>
          <li>1 egg white</li>
          <li>1 banana</li>
          <li>1 1/2 cup Old Fashioned oats</li>
          <li>2 Tbs organic honey</li>
          <li>2 tsp baking powder</li>
          <li>1 tsp vanilla extract</li>
          <li>1/4 tsp salt</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Blend the ingredients in a blender.</li>
        <li>Cook pancakes on the stove over medium heat.</li>
    </div>
  );
}

export default BananaPancakes;
