function BackButton() {
  return (
    <div className="mb-2">
      <a 
        className="text-title text-2xl pr-2 pb-2"
        href="/"
      >
        &lt;
      </a>
    </div>
  );
}

export default BackButton;
