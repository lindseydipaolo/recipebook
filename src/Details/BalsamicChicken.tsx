import React from 'react';
import BackButton from '../BackButton';

function BalsamicChicken() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <ul>balsamic glaze</ul>
          <li>1/4 cup extra virgin olive oil</li>
          <li>3 Tbs balsamic vinegar</li>
          <li>1 Tbs tomato paste</li>
          <li>1 tsp real honey</li>
          <li>juice of 1 lemon</li>
          <li>4-5 garlic cloves, minced (or 1 tsp of galic powder)</li>
          <li>1 Tbs thyme</li>
          <li>1 tsp dried oregano</li>
          <li>1/2 tsp paprika</li>
          <ul>chicken</ul>
          <li>~1 lb chicken breast</li>
          <ul>broccoli & bacon</ul>
          <li>1 head of broccoli, chopped into florets</li>
          <li>2 strips of turkey bacon, cut into ~1/4 inch pieces</li>
          <li>1 cup basmati rice</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Preheat the oven to 400 degrees.</li>
        <li>Mix the balsamic glaze ingredients in a bowl and set aside.</li>
        <li>Cook the basmati rice according to the instructions on the package.</li>
        <li>Prepare two baking pans, both with aluminum foil and non-stick spray.</li>
        <li>Place the chicken on one pan and drizzle with 1/2 the balsamic glaze.</li>
        <li>Bake the chicken in the oven for 30 minutes, or until the chicken is cooked all the way through.</li>
        <li>Chop the broccoli and turkey bacon and add it to the other pan.</li>
        <li>Drizzle the rest of the balsamic glaze over the broccoli and bacon.</li>
        <li>Bake the second pan in the oven for 20 minutes. It can go in the oven about 15 minutes after the first pan and finish at the same time.</li>
    </div>
  );
}

export default BalsamicChicken;
