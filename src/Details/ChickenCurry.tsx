import React from 'react';
import BackButton from '../BackButton';

function ChickenCurry() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>1 chicken breast, cut into ~1-inch cubes</li>
          <li>1 sweet onion, chopped into small bits</li>
          <li>1 green bell pepper, chopped into ~1 inch segments</li>
          <li>2 medium-sized sweet potatoes, cut into ~1-inch cubes</li>
          <li>2 cups oat milk</li>
          <li>1 handful of baby carrots, chopped into ~1 inch segments</li>
          <li>2 Tbs curry powder</li>
          <li>4 tsp garlic powder</li>
          <li>1/2 tsp salt</li>
          <li>1/4 tsp pepper</li>
          <li>2 Tbs olive oil</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Chop the chicken, onion, bell pepper, sweet potatoes, and carrots. Set them aside.</li>
        <li>Heat a large pan over medium heat.</li>
        <li>Add the onion and garlic powder. Sauté until the onion is lightly browned.</li>
        <li>Add the chicken and cook for 2-3 minutes.</li>
        <li>Add the salt, pepper, curry powder, and half the oat milk (1 cup).</li>
        <li>Stir the ingredients and cook for 1 minute.</li>
        <li>Add the sweet potatoes, bell pepper, carrots, and the rest of the oat milk.</li>
        <li>Bring the pan to a boil, then simmer for 10-15 minutes, or until the potatoes are soft.</li>
        <li>Serve over quinoa or brown rice.</li>
    </div>
  );
}

export default ChickenCurry;
