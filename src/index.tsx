import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Recipes from './Recipes';
import ChickpeaCurry from '../src/Details/ChickpeaCurry';
import AlmondFlourBread from './Details/AlmondFloudBread';
import CarrotMuffins from './Details/CarrotMuffins';
import BananaPancakes from './Details/BananaPancakes';
import ComingSoon from '../src/Details/ComingSoon';
import SweetPotatoSalad from './Details/SweetPotatoSalad';
import LemonMuffins from './Details/LemonMuffins';
import AlmondFlourBiscuit from './Details/AlmondFlourBiscuit';
import PumpkinMuffins from './Details/PumpkinMuffins';
import AutumnChicken from './Details/AutumnChicken';
import PumpkinPie from './Details/PumpkinPie';
import ChickenCurry from './Details/ChickenCurry';
import SausagePatties from './Details/SausagePatties';
import CinnamonPancakes from './Details/CinnamonPancakes';
import PumpkinPieOatmeal from './Details/PumpkinPieOatmeal';
import BalsamicChicken from './Details/BalsamicChicken';
import ApplesauceMuffins from './Details/ApplesauceMuffins';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<div className="min-h-screen bg-background"><Recipes /></div>}/>
        <Route path="/chickpeacurry" element={<div className="min-h-screen bg-background"><ChickpeaCurry /></div>}/>
        <Route path="/almondflourbread" element={<div className="min-h-screen bg-background"><AlmondFlourBread /></div>}/>
        <Route path="/sweetpotatosalad" element={<div className="min-h-screen bg-background"><SweetPotatoSalad /></div>}/>
        <Route path="/pumpkinmuffins" element={<div className="min-h-screen bg-background"><PumpkinMuffins /></div>}/>
        <Route path="/autumnchicken" element={<div className="min-h-screen bg-background"><AutumnChicken /></div>}/>
        <Route path="/carrotmuffins" element={<div className="min-h-screen bg-background"><CarrotMuffins /></div>}/>
        <Route path="/pumpkinpie" element={<div className="min-h-screen bg-background"><PumpkinPie /></div>}/>
        <Route path="/chickencurry" element={<div className="min-h-screen bg-background"><ChickenCurry /></div>}/>
        <Route path="/sausagepatties" element={<div className="min-h-screen bg-background"><SausagePatties /></div>}/>
        <Route path="/lemonmuffins" element={<div className="min-h-screen bg-background"><LemonMuffins /></div>}/>
        <Route path="/bananapancakes" element={<div className="min-h-screen bg-background"><BananaPancakes /></div>}/>
        <Route path="/almondflourbiscuits" element={<div className="min-h-screen bg-background"><AlmondFlourBiscuit /></div>}/>
        <Route path="/cinnamonpancakes" element={<div className="min-h-screen bg-background"><CinnamonPancakes /></div>}/>
        <Route path="/pumpkinpieoatmeal" element={<div className="min-h-screen bg-background"><PumpkinPieOatmeal /></div>}/>
        <Route path="/tacobowl" element={<div className="min-h-screen bg-background"><ComingSoon /></div>}/>
        <Route path="/balsamicchicken" element={<div className="min-h-screen bg-background"><BalsamicChicken /></div>}/>
        <Route path="/applesaucemuffins" element={<div className="min-h-screen bg-background"><ApplesauceMuffins /></div>}/>
        <Route path="/pestopasta" element={<div className="min-h-screen bg-background"><ComingSoon /></div>}/>
        <Route path="/beefstew" element={<div className="min-h-screen bg-background"><ComingSoon /></div>}/>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
