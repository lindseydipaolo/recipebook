import React from 'react';
import BackButton from '../BackButton';

function AppleSauceMuffins() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>1 1/2 cup almond flour</li>
          <li>2/3 cup applesauce</li>
          <li>2 eggs</li>
          <li>1/4 cup natural honey</li>
          <li>1 Tbs olive oil</li>
          <li>2 tsp apple cider vinegar</li>
          <li>1 tsp cinnamon</li>
          <li>1 tsp vanilla extract</li>
          <li>1 tsp baking powder</li>
          <li>1 tsp baking soda</li>
          <li>1/2 tsp salt</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Preheat the oven to 350 degrees Fahrenheit.</li>
        <li>Mix all the ingredient in a bowl.</li>
        <li>Spray a muffin tin with non-stick spray.</li>
        <li>Add the mixture to the tin and bake for 20 min.</li>
    </div>
  );
}

export default AppleSauceMuffins;
