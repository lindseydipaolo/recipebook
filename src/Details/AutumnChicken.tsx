import React from 'react';
import BackButton from '../BackButton';

function AutumnChicken() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>5 (6-7 oz) bone-in, skin on chicken thighs</li>
          <li>4 Tbs olive oil</li>
          <li>1 1/2 Tbs red wine vinegar</li>
          <li>3/4 tsp garlic powder</li>
          <li>1 Tbs each thyme, sage, rosemary</li>
          <li>Salt and pepper to taste</li>
          <li>1 large sweet potato</li>
          <li>1 head broccoli</li>
          <li>2 medium-size red fuji apples</li>
          <li>4 slices bacon</li>
          <li>2 Tbs parsley for garnish</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Preheat oven to 450 degrees.</li>
        <li>Add garlic powder, herbs, red wine vinegar, and half the olive oil (2 Tbs) to a large bowl and mix.</li>
        <li>Season the chicken with salt and pepper to taste, then place in the bowl and coat the chicken with the herb mixture. Allow it to sit.</li>
        <li>Chop the sweet potato and apples into roughly 1/2 inch cubes. Separate the head of broccoli into florets.</li>
        <li>Place the veggies and apples on a baking sheet and drizzle with remaining olive oil. Toss to coat evenly.</li>
        <li>Place the chicken thighs on top of the veggies and apples.</li>
        <li>Chop the bacon into pieces and sprinkle over the chicken/veggies/apples.</li>
        <li>Roast in the oven 30-35 minutes, until the chicken is cooked and the veggies and apples are soft.</li>
        <li>Garnish with parsley.</li>
    </div>
  );
}

export default AutumnChicken;
