import React from 'react';
import BackButton from '../BackButton';

function SausagePatties() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>1 lb ground turkey</li>
          <li>1 Tbs olive oil</li>
          <li>1 tsp fennel seeds</li>
          <li>1 tsp rubbed sage</li>
          <li>1 tsp garlic powder</li>
          <li>1 tsp salt</li>
          <li>1/8 tsp red pepper flakes</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Add all ingredients to a bowl.</li>
        <li>Mix the ingredients together until the spices are evenly distributed throughout the turkey.</li>
        <li>Optional: Let the mixture sit in the fridge, in an air-tight container, overnight for enhanced flavor.</li>
        <li>Heat a pan or cast iron skillet over medium heat.</li>
        <li>Shape the mixture into patties (there will be about 12 patties).</li>
        <li>Cook on each side until the patties are browned all the way through.</li>
    </div>
  );
}

export default SausagePatties;
