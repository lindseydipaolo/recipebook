import React from 'react';
import BackButton from '../BackButton';

function ComingSoon() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">Coming soon!</h1>
          {/* <li>1 Tbs olive oil</li>
          <li>1 1/2 cups cooked chickpeas</li>
          <li>1 1/4 Tbs curry powder</li>
          <li>1/2 cup coconut yogurt</li>
          <li>14 oz canned tomatoes</li>
          <li>2 Tbs nutritional yeast</li>
          <li>2 Tbs lime juice</li>
          <li>3 cups spinach (loosely packed)</li>
          <li>Salt and pepper to taste</li> */}
        </div>
      {/* <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Heat a skillet or pan and add the olive oil.</li>
        <li>Then add the chickpeas and curry powder; sauté for 2-3 minutes.</li>
        <li>Add the coconut yogurt, tomatoes, nutritional yeast, lime juice, and salt. Stir and let simmer for 5-10 minutes.</li>
        <li>Add the spinach and stir for 1-2 minutes.</li>
        <li>Season with salt & pepper to taste</li>
        <li>Serve over quinoa or brown rice.</li> */}
    </div>
  );
}

export default ComingSoon;
