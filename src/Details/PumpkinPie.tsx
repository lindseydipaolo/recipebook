import React from 'react';
import BackButton from '../BackButton';

function PumpkinPie() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>1 15-ounce can pure pumpkin puree</li>
          <li>1/3 cup almond flour</li>
          <li>1/3 cup pure maple syrup</li>
          <li>3/4 cup + 2 Tbs oat milk</li>
          <li>2 Tbsp olive oil</li>
          <li>2 Tbsp ground flaxseed</li>
          <li>2 tsp cinnamon</li>
          <li>2 tsp baking powder</li>
          <li>1 tsp pumpkin pie spice</li>
          <li>1/2 tsp salt</li>
          <li>2 1/2 tsp vanilla extract</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Preheat the oven to 400 degrees Fahrenheit.</li>
        <li>Stir all of the ingredients together in a large bowl.</li>
        <li>Spray a 9-inch round pan with non-stick spray.</li>
        <li>Bake the ingredients in the pan for 35 minutes. The pie will be very soft, which is okay!</li>
        <li>Allow the pie to chill in the refrigerator overnight, or for at least 6 hours.</li>
    </div>
  );
}

export default PumpkinPie;
