import React from 'react';
import BackButton from '../BackButton';

function PumpkinMuffins() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <ul>dry ingredients</ul>
          <li>1 1/2 cups oat flour, made from Old Fashioned oats</li>
          <li>1/4 cup Old Fashioned oats</li>
          <li>1 1/2 tsp cinnamon</li>
          <li>1 tsp baking soda</li>
          <li>1/4 cup walnuts</li>
          <ul>wet ingredients</ul>
          <li>1 egg</li>
          <li>1 cup natural honey</li>
          <li>1/4 cup almond milk</li>
          <li>1 tsp vanilla extract</li>
          <li>1 cup pumpkin puree</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Preheat the oven to 350 degrees.</li>
        <li>Mix the dry ingredients together in a large bowl, except for the walnuts.</li>
        <li>Lightly mix the wet ingredients together in a separate bowl, except for the pumpkin puree.</li>
        <li>Make a hole in the center of the dry ingredients and pour in the wet ingredients. Fold the ingredients together with a spatula. Avoid over-mixing.</li>
        <li>Add the pumpkin puree to the bowl and fold it in until everything is combined.</li>
        <li>Generously spray a muffin tin with non-stick spray, then fill each cup about 3/4 full.</li>
        <li>Sprinkle the muffins with walnuts. Bake for 18-20 minutes, until the muffin tops are golden brown.</li>
    </div>
  );
}

export default PumpkinMuffins;
