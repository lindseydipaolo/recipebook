import React from 'react';
import BackButton from '../BackButton';

function SweetPotatoSalad() {
  return (
    <div className="p-4">
      <BackButton />
      <div className="mb-4">
        <h1 className="text-3xl text-title font-sans mb-2">ingredients</h1>
          <li>4 medium-sized sweet potatoes</li>
          <li>1 Tbs olive oil</li>
          <li>1 cup chopped spinach</li>
          <li>1/2 cup chopped red onion</li>
          <li>2 Tbs apple cider vinegar</li>
          <li>2 Tbs lemon juice</li>
          <li>1 chopped avocado</li>
        </div>
      <h1 className="text-3xl text-title font-sans mb-2">instructions</h1>
        <li>Cut the sweet potatoes into half-inch cubes, leaving the skins on.</li>
        <li>Heat a pan over medium heat, then add the olive oil.</li>
        <li>Add the chopped sweet potatoes to the pan and cover. Flip and stir every few minutes until the potatoes can be punctured with a fork.</li>
        <li>While the potatoes cook, add the spinach, red onion, apple cider vinegar, lemon juice, and avocado to a bowl.</li>
        <li>Once cooked, let the sweet potatoes cool. Then add them to the bowl along with salt & pepper to taste.</li>
    </div>
  );
}

export default SweetPotatoSalad;
